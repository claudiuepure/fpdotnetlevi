﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PublicTransport.Domain.Buses
{
    public class Route
    {
        public int Id { get; set; }

        public int Number { get; set; }
    }
}
