﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PublicTransport.Domain.Buses
{
    public interface IRouteRepository
    {
        IEnumerable<Route> GetRoutes();

        Route GetRoute(int id);

        IEnumerable<RouteStationMap> GetMappingsToStations(int routeId);
    }
}
