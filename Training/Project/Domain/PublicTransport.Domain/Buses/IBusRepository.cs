﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Domain.Buses
{
    public interface IBusRepository
    {
        IEnumerable<Bus> GetBuses();

        Bus GetBus(int id);

        bool DeleteBus(int id);
    }
}