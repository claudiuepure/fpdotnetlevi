﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Domain.Buses
{
    public class Bus
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int RouteId { get; set; }
    }
}