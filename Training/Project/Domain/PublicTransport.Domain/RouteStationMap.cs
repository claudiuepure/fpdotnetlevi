﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PublicTransport.Domain
{
    public class RouteStationMap
    {
        public int RouteId { get; set; }

        public int StationId { get; set; }
    }
}
