﻿using Dapper;
using PublicTransport.Domain;
using PublicTransport.Domain.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PublicTransport.Repository
{
    public class RouteRepository : IRouteRepository
    {
        public IEnumerable<Route> GetRoutes()
        {
            using (var connection = Storage.GetOpenConnection())
            {
                var routes = connection.Query<Route>("select * from route");
                return routes;
            }
        }

        public Route GetRoute(int id)
        {
            using (var connection = Storage.GetOpenConnection())
            {
                var route = connection.Query<Route>("select * from route where Id=@id", new { id = id }).FirstOrDefault();
                return route;
            }
        }

        public IEnumerable<RouteStationMap> GetMappingsToStations(int routeId)
        {
            using (var connection = Storage.GetOpenConnection())
            {
                var routeStations = connection.Query<RouteStationMap>("select * from route_station_map where RouteId=@id", new { id = routeId });
                return routeStations;
            }
        }
    }
}