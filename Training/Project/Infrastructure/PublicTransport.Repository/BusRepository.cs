﻿using Dapper;
using MySql.Data.MySqlClient;
using PublicTransport.Domain.Buses;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Repository
{
    public class BusRepository : IBusRepository
    {
        public IEnumerable<Bus> GetBuses()
        {
            using (var connection = Storage.GetOpenConnection())
            {
                var buses = connection.Query<Bus>("select * from bus");
                return buses;
            }
        }

        public Bus GetBus(int id)
        {
            using (var connection = Storage.GetOpenConnection())
            {
                var bus = connection.Query<Bus>("select * from bus where Id=@id", new { id = id }).FirstOrDefault();
                return bus;
            }
        }

        public int UpdateBus(Bus bus)
        {
            using (var connection = Storage.GetOpenConnection())
            {
                string sqlQuery =
                    "UPDATE bus " +
                         "SET description = @description " +
                    "WHERE Id = @Id";
                int rowsAffected = connection.Execute(sqlQuery, bus);
                return rowsAffected;
            }
        }

        public int UpdateBus(int id, string descsription)
        {
            using (var connection = Storage.GetOpenConnection())
            {
                string sqlQuery =
                    "UPDATE bus " +
                         "SET description = @description " +
                    "WHERE Id = @id";
                int rowsAffected = connection.Execute(sqlQuery, new { id=id, description=descsription});
                return rowsAffected;
            }
        }

        public bool DeleteBus(int id)
        {
            using (var connection = Storage.GetOpenConnection())
            {
                var isSuccess = connection.Execute("delete from bus where Id=@id", new { Id = id });
                return isSuccess>0;
            }
        }
    }
}