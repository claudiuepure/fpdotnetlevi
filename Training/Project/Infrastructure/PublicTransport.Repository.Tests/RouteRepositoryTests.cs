﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Repository.Tests
{
    [TestFixture]
    public class RouteRepositoryTests
    {
        [Test]
        public void Test()
        {
            Assert.DoesNotThrow(() =>
            {
                var sut = new RouteRepository();
                var stations = sut.GetMappingsToStations(1);
            });
        }
    }
}
