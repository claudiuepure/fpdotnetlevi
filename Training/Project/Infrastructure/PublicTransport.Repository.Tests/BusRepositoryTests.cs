﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Repository.Tests
{
    [TestFixture]
    public class BusRepositoryTests
    {
        [Test]
        public void GetBuses_WhenQuery_Expect_NoException()
        {
            Assert.DoesNotThrow(() =>
            {
                var sut = new BusRepository();
                var buses = sut.GetBuses();
            });
        }
    }
}