﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PublicTransport.Application.Models
{
    public class RouteDTO
    {
        public string Id { get; set; }

        public string Number { get; set; }
    }
}