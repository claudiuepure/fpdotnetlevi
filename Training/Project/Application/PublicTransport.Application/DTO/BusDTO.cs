﻿using PublicTransport.Domain.Buses;
using System;
using System.Collections.Generic;
using System.Text;

namespace PublicTransport.Application.Models
{
    public class BusDTO
    {
        public BusDTO()
        {
            this.Route = new RouteDTO();
        }

        public string Id { get; set; }

        public string Description { get; set; }

        public RouteDTO Route { get; set; }
    }
}
