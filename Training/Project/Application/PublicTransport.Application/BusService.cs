using PublicTransport.Application.Models;
using PublicTransport.Domain.Buses;
using PublicTransport.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PublicTransport.Application
{
    public class BusService
    {
        private IBusRepository busRepository;
        private IRouteRepository routeRepository;

        public BusService()
        {
            this.busRepository = new BusRepository();
            this.routeRepository = new RouteRepository();
        }

        public IEnumerable<BusDTO> GetBuses()
        {
            var buses = this.busRepository.GetBuses();
            var busModels = buses.Select(this.MapToBusDTO);
            return busModels;
        }

        public BusDTO GetBus(string id)
        {
            var idAsInt = int.Parse(id);
            var bus = this.busRepository.GetBus(idAsInt);
            var busModel = this.MapToBusDTO(bus);

            return busModel;
        }

        public bool DeleteBus(string id)
        {
            var idAsInt = int.Parse(id);
            return this.busRepository.DeleteBus(idAsInt);
        }

        public void CreateBus(BusDTO bus)
        {
            throw new NotImplementedException();
        }

        public void UpdateBus(BusDTO candidateBus)
        {
            throw new NotImplementedException();
        }

        private RouteDTO MapToRouteDTO(Route route)
        {
            var routeModel = new RouteDTO
            {
                Id = route.Id.ToString(),
                Number = route.Number.ToString()
            };

            return routeModel;
        }

        private BusDTO MapToBusDTO(Bus bus)
        {
            var busDTO = new BusDTO
            {
                Id = bus.Id.ToString(),
                Description = bus.Description,
            };

            var route = this.routeRepository.GetRoute(bus.RouteId);
            var routeDTO = this.MapToRouteDTO(route);
            busDTO.Route = routeDTO;

            return busDTO;
        }
    }
}