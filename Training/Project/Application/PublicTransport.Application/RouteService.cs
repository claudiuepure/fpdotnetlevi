﻿using PublicTransport.Application.Models;
using PublicTransport.Domain.Buses;
using PublicTransport.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PublicTransport.Application
{
    public class RouteService
    {
        private IRouteRepository routeRepository;

        public RouteService()
        {
            this.routeRepository = new RouteRepository();
        }

        public IEnumerable<RouteDTO>GetRoutes()
        {
            var routes = this.routeRepository.GetRoutes();
            var routeModels = routes.Select(this.MapToRouteModel);
            return routeModels; ;
        }

        private RouteDTO MapToRouteModel(Route route)
        {
            var routeModel = new RouteDTO
            {
                Id = route.Id.ToString(),
                Number = route.Number.ToString()
            };

            return routeModel;
        }
    }
}
