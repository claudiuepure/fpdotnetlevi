﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PublicTransport.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PublicTransport.UI.Models
{
    public class CreateBusModel
    {
        public CreateBusModel()
        {
            this.Bus = new BusModel();
            this.SelectedRoute = string.Empty;
            this.Routes = new List<RouteDTO>();
        }

        public BusModel Bus { get; set; }

        public string BusId => this.Bus.Id;

        public string BusDescription => this.Bus.Description;

        public string SelectedRoute { get; set; } = string.Empty;
        //{
        //    get
        //    {
        //        return this.Bus.Route.Id;
        //    }

        //    set
        //    {
        //        this.Bus.Route.Id = value;
        //    }
        //}

        public IList<RouteDTO> Routes { get; set; }

        public IEnumerable<SelectListItem> RouteItems
        {
            get
            {
                var routeItems = this.Routes.Select(r => new SelectListItem
                {
                    Value = r.Id,
                    Text = r.Number,
                    Selected = this.Bus.Route.Id == r.Id
                });

                return routeItems;
            }
        }
    }
}
