﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PublicTransport.Application;
using PublicTransport.UI.Models;

namespace PublicTransport.UI.Controllers
{
    public class BusController : Controller
    {
        private BusService busService = new BusService();
        private RouteService routeService = new RouteService();

        // GET: Bus
        public ActionResult Index()
        {
            var buses = busService.GetBuses();

            return View(buses);
        }

        // GET: Bus/Details/5
        public ActionResult Details(int id)
        {
            var bus = busService.GetBus(id);
            return View(bus);
        }

        // GET: Bus/Create
        public ActionResult Create()
        {
            var routeModels = this.routeService.GetRoutes();

            var createBusModel = new CreateBusModel();
            createBusModel.Routes = routeModels.ToList();

            return View(createBusModel);
        }

        // POST: Bus/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Bus/Edit/5
        public ActionResult Edit(int id)
        {
            var bus = busService.GetBus(id);
            var routes = routeService.GetRoutes();
            //bus.Routes = routes.Select(r => new SelectListItem
            //{
            //    Value = r.Id,
            //    Text = r.Number,
            //    Selected = bus.Route.Id == r.Id
            //});
            //bus.SelectedRoute = bus.Route.Id;
            return View(bus);
        }

        // POST: Bus/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Bus/Delete/5
        public ActionResult Delete(int id)
        {
            var bus = busService.GetBus(id);
            return View(bus);
        }

        // POST: Bus/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                busService.DeleteBus(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}