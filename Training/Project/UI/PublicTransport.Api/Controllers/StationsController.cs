﻿using Microsoft.AspNetCore.Mvc;

namespace PublicTransport.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Stations")]
    public class StationsController : Controller
    {
        //private IStorage storage;

        //public StationsController(IStorage storage)
        //{
        //    this.storage = storage;
        //}

        //// GET: api/Stations
        //[HttpGet]
        //public IEnumerable<Station> Get()
        //{
        //    var stations = storage.Stations;
        //    return stations;
        //}

        //// GET: api/Stations/5
        //[HttpGet("{id}")]
        //public Station Get(string id)
        //{
        //    var station = storage.Stations.FirstOrDefault(s => s.Id.Equals(id));
        //    return station;
        //}

        //// POST: api/Stations
        //[HttpPost]
        //public void Post([FromBody]Station station)
        //{
        //    station.Id = Guid.NewGuid().ToString();
        //    this.storage.Stations.Add(station);
        //}

        //// PUT: api/Stations/5
        //[HttpPut("{id}")]
        //public void Put(string id, [FromBody]Station station)
        //{
        //    var candidateStation = this.storage.Stations.FirstOrDefault(s => s.Id.Equals(id));
        //    if (candidateStation == null)
        //    {
        //        candidateStation = new Station()
        //        {
        //            Id = id,
        //            Name = station.Name
        //    };

        //        this.storage.Stations.Add(candidateStation);
        //    }
        //    else
        //    {
        //        candidateStation.Name = station.Name;           
        //    }
        //}
        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(string id)
        //{
        //    var candidateStation = this.storage.Stations.FirstOrDefault(s => s.Id.Equals(id));
        //    this.storage.Stations.Remove(candidateStation);
        //}
    }
}