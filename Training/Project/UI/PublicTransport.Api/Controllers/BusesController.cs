﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using PublicTransport.Application;
using PublicTransport.Application.Models;

namespace PublicTransport.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Buses")]
    public class BusesController : Controller
    {
        private BusService busService;

        public BusesController()
        {
            this.busService = new BusService();
        }

        // GET: api/Buses
        [HttpGet]
        public IEnumerable<BusDTO> Get()
        {
            var buses = this.busService.GetBuses();
            return buses;
        }

        // GET: api/Buses/5
        [HttpGet("{id}", Name = "Get")]
        public BusDTO Get(string id)
        {
            var bus = this.busService.GetBus(id);
            return bus;
        }
        
        // POST: api/Buses
        [HttpPost]
        public void Post([FromBody]BusDTO bus)
        {
            this.busService.CreateBus(bus);
        }
        
        // PUT: api/Buses/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]BusDTO bus)
        {
            var candidateBus = this.busService.GetBus(bus.Id);
            if (candidateBus == null)
            {
                candidateBus = new BusDTO()
                {
                    Id = id,
                    Description = bus.Description,
                    Route = bus.Route
                };

                this.busService.CreateBus(candidateBus);
            }
            else
            {
                candidateBus.Id = bus.Id;
                candidateBus.Description = bus.Description;
                candidateBus.Route = bus.Route;

                this.busService.UpdateBus(candidateBus);
            }
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            this.busService.DeleteBus(id);
        }
    }
}
