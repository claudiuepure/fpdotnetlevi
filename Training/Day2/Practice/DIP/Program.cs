﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.DIP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var shop = new Bad.Shop();
            shop.ListProduct();


            var provider = new Good.Provider();

            var goodShop = new Good.Shop(provider);
            shop.ListProduct();
        }
    }
}
