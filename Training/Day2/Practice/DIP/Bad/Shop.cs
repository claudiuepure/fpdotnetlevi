﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.DIP.Bad
{
    // High-level class
    public class Shop
    {
        // dependency on lower-level class
        private Provider provider;

        public Shop()
        {
            this.provider = new Provider();
        }

        public void ListProduct()
        {
            var producst = this.provider.GetProducts();
            foreach (var p in producst)
            {
                Console.WriteLine(p);
            }
        }
    }
}
