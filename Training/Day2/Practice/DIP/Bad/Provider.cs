﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.DIP.Bad
{
    // Low-level class
    public class Provider
    {
        public string Name { get; set; }

        public List<string> GetProducts()
        {
            var products = new List<string> { "pc", "printer", "laptop" };
            return products;
        }
    }
}
