﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.DIP.Good
{
    // Rewrite the following snippet using the DIP.
    // High-level class
    public class Shop
    {
        // dependency on lower-level class should be replaced by a
        // dependency on a a provider abstraction
        private IProvider provider;

        public Shop(IProvider p)
        {
            // a provider implementation should be passed from outside the class
            this.provider = p;
        }

        public void ListProduct()
        {
            var producst = this.provider.GetProducts();
            foreach (var p in producst)
            {
                Console.WriteLine(p);
            }
        }
    }
}