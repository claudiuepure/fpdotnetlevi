﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.DIP.Good
{
    public interface IProvider
    {
        List<string> GetProducts();
    }
}
