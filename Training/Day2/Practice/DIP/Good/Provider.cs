﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.DIP.Good
{
    // Low-level class
    public class Provider : IProvider
    {
        public string Name { get; set; }

        public List<string> GetProducts()
        {
            var products = new List<string> { "pc", "printer", "laptop" };
            return products;
        }
    }
}
