﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.LSP.Good
{
    public class Plane : FlyingObject
    {
        public override void AfterStart()
        {
            this.ShowStaus();
        }

        private void ShowStaus()
        {
            if (this.IsEngineOn)
            {
                Console.WriteLine("Plane is ready.");
            }
            else
            {
                Console.WriteLine("Plane is not ready.");
            }
        }

        public static void Main()
        {
            var plane = new Plane();
            plane.Fly();
        }
    }
}
