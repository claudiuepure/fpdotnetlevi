﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.LSP.Good
{
    // This class, since is a base class, it is recommended to be abstract
    public class FlyingObject
    {
        public bool IsEngineOn { get; private set; }

        public void Fly()
        {
            this.BeforeStart();
            this.IsEngineOn = true;
            this.AfterStart();
        }

        public virtual void BeforeStart()
        {
        }

        public virtual void AfterStart()
        {
        }
    }
}
