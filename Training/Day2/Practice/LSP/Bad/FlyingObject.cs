﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.LSP.Bad
{
    public class FlyingObject
    {
        public bool IsEngineOn { get; private set; }

        public virtual void Fly()
        {
            this.IsEngineOn = true;
        }
    }
}
