﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.LSP.Bad
{
    public class Plane : FlyingObject
    {
        public override void Fly()
        {
            // How does this class know about what base class Fly does?
            // If base.Fly() is omitted, the LSP is broken.
            base.Fly();
            // How does this class know which method should be called first?
            // base.Fly or this.SetStatus?
            this.ShowStaus();
        }

        private void ShowStaus()
        {
            if (this.IsEngineOn)
            {
                Console.WriteLine("Plane is ready.");
            }
            else
            {
                Console.WriteLine("Plane is not ready.");
            }
        }
    }
}
