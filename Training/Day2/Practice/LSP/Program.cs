﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.LSP
{
    class Program
    {
        public static void Main()
        {
            var badFlyingObject = new Bad.FlyingObject();
            FlyAnObject(badFlyingObject);

            var badPlane = new Bad.Plane();
            FlyAnObject(badPlane);

            var goodFLyingObject = new Good.FlyingObject();
            FlyAnObject(goodFLyingObject);

            var goodPlane = new Good.Plane();
            FlyAnObject(goodPlane);
        }

        public static void FlyAnObject(Bad.FlyingObject flyingObject)
        {
            flyingObject.Fly();
            Console.WriteLine($"Engine is turned on: {flyingObject.IsEngineOn}");
        }

        public static void FlyAnObject(Good.FlyingObject flyingObject)
        {
            flyingObject.Fly();
            Console.WriteLine($"Engine is turned on: {flyingObject.IsEngineOn}");
        }
    }
}
