﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.OCP.Good
{
    //implement polymorphism for OCP
    class GraphicEditor
    {
        public void drawShape(Shape s)
        {
            s.draw();
        }
    }
}
