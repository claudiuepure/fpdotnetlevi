﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.OCP.Good
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var circle = new Circle();
            var rectangle = new Rectangle();
            var graphics = new GraphicEditor();

            graphics.drawShape(circle);
            graphics.drawShape(rectangle);

            Console.ReadKey();
        }
    }
}
