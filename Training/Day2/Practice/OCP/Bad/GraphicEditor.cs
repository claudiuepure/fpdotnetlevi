﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.OCP
{
    class GraphicEditor
    {
        public void drawShape(Shape s)
        {
            //s.draw();
            if (s is Circle)
            {
                drawCircle((Circle)s);
            }
            else if(s is Rectangle)
            {
                drawRectangle((Rectangle)s);
            }

            
        }
        public void drawCircle(Circle r)
        {
        }
        public void drawRectangle(Rectangle r)
        {
        }
    }

}
