﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.SRP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var math = new Bad.Math();
            math.Add(3, 4);
            math.PrintSum();
        }
    }
}
