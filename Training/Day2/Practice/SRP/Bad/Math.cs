﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.SRP.Bad
{
    // this class has more than one reason to change
    public class Math
    {
        private int sum;

        // first reason to change
        public int Add(int x, int y)
        {
            // this method does more than one thing:
            // 1. computes the sum
            // 2. saves the value in this.sum field

            var sum = x + y;
            this.sum = sum;
            return sum;
        }

        // second reason to change
        public void PrintSum()
        {
            Console.WriteLine($"Suma est: {this.sum}");
        }
    }
}
