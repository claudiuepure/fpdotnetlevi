﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.SRP.Bad
{
    public class Student
    {
        private string name;

        private DateTime birthday;

        public Student(string name, DateTime birthday)
        {
            this.name = name;
            if (this.ValidateBirthday(birthday))
            {
                this.birthday = birthday;
            }
            else
            {
                this.birthday = DateTime.MinValue;
            }
        }
        // validation should be separated in a different class
        // one thing
        private bool ValidateBirthday(DateTime date)
        {
            if (date < DateTime.Now)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // another thing
        public int GetAge()
        {
            return DateTime.Now.Year - this.birthday.Year;
        }
    }
}
