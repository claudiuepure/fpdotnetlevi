﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.SRP.Good
{
    public class Student 
    {
        private string name;
        private Validator validator = new Validator();
        private DateTime birthday;

        public Student(string name, DateTime birthday)
        {
            this.name = name;
            // instead of a method call, it should be a validator method call
            var result = validator.ValidateBirthday(birthday);
            if(result)
            {
                this.birthday = birthday;
            }
            else
            {
                this.birthday = DateTime.MinValue;
            }
        }

        // validation should be separated in a different class
        // one thing
       

        // another thing
        public int GetAge()
        {
            return DateTime.Now.Year - this.birthday.Year;
        }
    }
}
