﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.ISP.Bad
{
    class AllInOnePrinter : ISmartDevice
    {
        public void Print()
        {
            // Printing code.
        }
        public void Fax()
        {
            // Beep booop biiiiip.
        }
        public void Scan()
        {
            // Scanning code.
        }
    }
}
