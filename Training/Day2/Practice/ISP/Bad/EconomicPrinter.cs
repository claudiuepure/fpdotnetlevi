﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.ISP.Bad
{
    //Now suppose we need to handle a dumb device (EconomicPrinter class)
    //that can only print
    //this class is forced to implement methods that dont use
    class EconomicPrinter : ISmartDevice
    {
        public void Print()
        {
            //Yes I can print.
        }
        public void Fax()
        {
            throw new NotSupportedException();
        }
        public void Scan()
        {
            throw new NotSupportedException();
        }
    }
}
