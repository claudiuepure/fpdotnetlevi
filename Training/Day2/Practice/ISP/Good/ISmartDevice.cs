﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.ISP.Good
{
    //split this interface in multiple
    interface ISmartDevice
    {
        void Print();
        void Fax();
        void Scan();
    }
}
