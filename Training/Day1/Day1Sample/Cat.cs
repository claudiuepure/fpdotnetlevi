﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1Sample
{
    public class Cat : Animal
    {

        public int Age;
        public int Height;

        // This is a Constructor with 3 parameters.
        // Using :base(name) to call constructor of parent: Animal(string).
        // The fields of superclass will be assigned values.
        // Then the fields of this class is assigned values.
        public Cat(string name, int Age, int Height)
            : base(name)
        {
            this.Age = Age;
            this.Height = Height;
            Console.WriteLine("- Cat(string,int,int)");
        }

        // This constructor call to default constructor of superclass.
        public Cat(int Age, int Height)
            : base()
        {
            this.Age = Age;
            this.Height = Height;
            Console.WriteLine("- Cat(int,int)");
        }

        public void Say()
        {
            Console.WriteLine("Meo");
        }

        // Override method in parent class.
        // Rewrite this method,
        // to accurately describe the behavior of the cat.
        public override void Move()
        {
            Console.WriteLine("Cat Move ...");
        }
    }
}
