﻿using System;

namespace Day1Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            //Thi will not work abstract classes cannot be instantiated
            //Animal pet = new Animal();

            Console.WriteLine("Create Cat object from Cat(string,int,int)");

            // Create a Cat object via Constructor with 3 parameters:
            // Name of Animal assigned the value "Tom".
            // Age of Cat assigned the value 3
            // Height of Cat assigned the value 20.
            Cat tom = new Cat("Tom", 3, 20);

            Console.WriteLine("------");

            Console.WriteLine("Name = {0}", tom.Name);
            Console.WriteLine("Age = {0}", tom.Age);
            Console.WriteLine("Height = {0}", tom.Height);

            Console.WriteLine("------");

            // Call method inherited from Animal
            tom.Sleep();

            // Call Say() method (of Cat)
            tom.Say();
            //create mouse object
            Console.WriteLine("Create mouse object from Mouse()");
            Animal mouse = new Mouse();

            Console.WriteLine("Create mouse object from Mouse(string,int)");
            var jerry = new Mouse("Jerry", 10);

            Console.WriteLine("------");

            Console.WriteLine("Name = {0}", jerry.Name);
            Console.WriteLine("Height = {0}", jerry.Weight);

            Console.WriteLine("------");
            // Using the operator 'is' to check the type of an object
            bool isMouse = tom is Mouse;// false

            Console.WriteLine("Tom is mouse? " + isMouse);

            bool isCat = tom is Cat; // true
            Console.WriteLine("Tom is cat? " + isCat);

            bool isAnimal = tom is Animal; // true
            Console.WriteLine("Tom is animal? " + isAnimal);

            isMouse = jerry is Mouse;

            Console.WriteLine("Jerry is mouse? " + isMouse);

            isCat = jerry is Cat; 
            Console.WriteLine("Jerry is cat? " + isCat);

            isAnimal = jerry is Animal; 
            Console.WriteLine("Jerry is animal? " + isAnimal);
        }
    }
}
