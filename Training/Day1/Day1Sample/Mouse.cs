﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1Sample
{
    public class Mouse : Animal
    {

        private int weight;

        public int Weight
        {
            get
            {
                return this.weight;
            }
            set
            {
                this.weight = value;
            }
        }

        // Default constructor (Without parameter).
        // Call to Mouse(int)
        public Mouse()
            : this(100)
        {
            Console.WriteLine("- Mouse()");
        }

        // This Constructor has one parameter.
        // And not specified 'base'.
        // This constructor based from default Constructor of parent class.
        public Mouse(int Weight)
        {
            Console.WriteLine("- Mouse(int)");
            this.Weight = Weight;
        }

        // This Constructor has 2 parameters.
        public Mouse(String name, int Weight)
            : base(name)
        {
            Console.WriteLine("- Mouse(string, int)");
            this.Weight = Weight;
        }
    }

}
