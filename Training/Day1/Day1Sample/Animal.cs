﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1Sample
{
    public abstract class Animal
    {
        // This is Name field.
        // for example, Tom cat, Jerry mouse.
        private string name;

        //this is a  provides a flexible mechanism to read, write, 
        //or compute the value of a private field
        //docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        // Default constructor.
        public Animal()
        {
            Console.WriteLine("- Animal()");
        }

        public Animal(string Name)
        {
            // Assign a value to the Name field.
            this.Name = Name;
            Console.WriteLine("- Animal(string)");
        }

        // Move(): Animal Move.
        // Virtual: Allows the subclass to override this method.
        public virtual void Move()
        {
            Console.WriteLine("Animal Move");
        }

        public void Sleep()
        {
            Console.WriteLine("Sleep");
        }

    }
}
