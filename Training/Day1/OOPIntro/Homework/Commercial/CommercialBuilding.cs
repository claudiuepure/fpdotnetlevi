﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Commercial
{
    // TODO 1: Model a commercial building, with the following requirements:
    //
    // a commercial building is a building and should follow all the building rules 
    // the class should be called [CommercialBuilding]
    // the class cannot be instantiated.
    // the class should be the base for other concrete classes
    // the class should have a [program] field of type string that should be available for subclasses
    // all its subclasses will have a method [GetDailyClientsNumber] 
    //     that will randomly generate an int value, represententing the number of clients who enter the building in a day;
    //     the subclasses can change the behavior of [GetDailyClientsNumber] method
    // all its subclasses must define a method called [GetDailyProfit] that returns the profit using a formula based on the clients number
    //
    public class CommercialBuilding
    {
       
    }
}
