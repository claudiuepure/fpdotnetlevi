﻿using OOPIntro.Homework.Educational;
using OOPIntro.Homework.Residential;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            var ap2rooms = new Apartment("Independentei", 900);
            var ap3rooms = new Apartment("Tudor Vladimirescu", 1300);

            var naturalHistoryMuseum = new Museum("Independentei", 12000, 36500);

            var schoolEminescu = new School("Mihail Kogalniceanu", 24000);

            var dochia = new MountainHut("Masivul Ceahlau", 150000, 6);

            var buildings = new List<Building>()
            {
                ap2rooms,
                ap3rooms,
                naturalHistoryMuseum,
                schoolEminescu,
                dochia
            };

            foreach (var building in buildings)
            {
                Console.WriteLine(
                    $"\n Building on address {building.AddressUppercase}: " +
                    $"\n yearly income: {building.GetIncome()} " +
                    $"\n monthly income: {building.GetMonthlyIncome()}");
            }
        }
    }
}
