﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework
{
    public abstract class Building
    {
        protected string address;

        protected Building(string address)
        {
            this.address = address;
        }

        public string AddressUppercase 
        {
            get
            {
                return this.address.ToUpper();
            }
        }

        public abstract double GetIncome();

        public virtual double GetMonthlyIncome()
        {
            var income = this.GetIncome();
            var monthlyIncome = income / 12;
            return monthlyIncome;
        }
    }
}