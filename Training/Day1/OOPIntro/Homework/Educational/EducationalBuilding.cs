﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Educational
{
    public abstract class EducationalBuilding : Building
    {
        protected double incomeFromGovernment;

        protected EducationalBuilding(string address, double incomeFromGovernment) 
            : base(address)
        {
            this.incomeFromGovernment = incomeFromGovernment;
        }

        public override double GetIncome()
        {
            return this.incomeFromGovernment;
        }
    }
}
