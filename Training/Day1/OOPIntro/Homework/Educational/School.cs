﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Educational
{
    public class School : EducationalBuilding
    {
        public School(string address, double incomeFromGoverment) 
            : base(address, incomeFromGoverment)
        {
        }

        public override double GetIncome()
        {
            return this.incomeFromGovernment;
        }

        public override double GetMonthlyIncome()
        {
            var income = this.GetIncome();
            var monthlyIncome = income / 9;
            return monthlyIncome;
        }
    }
}
