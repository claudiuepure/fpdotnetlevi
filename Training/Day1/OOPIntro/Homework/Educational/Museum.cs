﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Educational
{
    public class Museum : EducationalBuilding
    {
        private double incomeFromTax;

        public Museum(string address, double incomeFromGovernment, double incomeFromTax) 
            : base(address, incomeFromGovernment)
        {
            this.incomeFromTax = incomeFromTax;
        }

        public override double GetIncome()
        {
            var income = base.GetIncome() + this.incomeFromTax;
            return income;
        }
    }
}
