﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Residential
{
    public abstract class ResidentialBuilding : Building
    {
        protected double rent;

        protected ResidentialBuilding(string address, double rent) 
            : base(address)
        {
            this.rent = rent;
        }

        public override double GetMonthlyIncome()
        {
            return this.rent;
        }
    }
}
