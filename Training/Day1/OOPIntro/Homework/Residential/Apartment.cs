﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Residential
{
    public class Apartment : ResidentialBuilding
    {
        public Apartment(string address, double rent)
            : base(address, rent)
        {
        }

        public override double GetIncome()
        {
            var income = this.rent * 12;
            return income;
        }
    }
}
