﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntro.Homework.Residential
{
    public class MountainHut : ResidentialBuilding
    {
        private int numberOfMonthsOpen;

        public MountainHut(string address, double rent, int numberOfMonthsOpen) 
            : base(address, rent)
        {
            this.numberOfMonthsOpen = numberOfMonthsOpen;
        }

        public override double GetIncome()
        {
            var income = this.rent * this.numberOfMonthsOpen;
            return income;
        }
    }
}
