﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicTransport.WebApi.Infrastructure
{
    public interface IStorage
    {
        IList<Bus> Buses { get; }

        IList<Station> Stations { get; }
    }
}
