﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicTransport.WebApi.Infrastructure
{
    public class Storage : IStorage
    {
        public IList<Bus> Buses { get; }

        public IList<Station> Stations { get; }


        public Storage()
        {
            this.Buses = new List<Bus>
            {
                new Bus
                {
                    Id = Guid.NewGuid().ToString(),
                    Number = 41
                },
                new Bus
                {
                    Id = Guid.NewGuid().ToString(),
                    Number = 28
                }
            };

            this.Stations = new List<Station>
            {
                new Station
                {
                    Id = Guid.NewGuid().ToString(),
                    Name="Podu Ros"
                },
                 new Station
                {
                    Id = Guid.NewGuid().ToString(),
                    Name="Copou"
                },
                 new Station
                {
                    Id = Guid.NewGuid().ToString(),
                    Name="Tg. Cucu"
                }
            };
        }
    }
}
