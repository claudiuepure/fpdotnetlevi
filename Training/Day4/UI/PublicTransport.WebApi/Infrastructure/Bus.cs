﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicTransport.WebApi.Infrastructure
{
    public class Bus
    {
        public Bus()
        {
            this.Stations = new List<string>();
        }

        public string Id { get; set; }

        public int Number { get; set; }

        public IList<string> Stations { get; set; }
    }
}
