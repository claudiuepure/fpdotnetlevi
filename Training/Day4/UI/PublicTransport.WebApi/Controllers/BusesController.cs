﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicTransport.WebApi.Infrastructure;

namespace PublicTransport.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Buses")]
    public class BusesController : Controller
    {
        private IStorage storage;

        public BusesController(IStorage storage)
        {
            this.storage = storage;
        }

        // GET: api/Buses
        [HttpGet]
        public IEnumerable<Bus> Get()
        {
            var buses = storage.Buses;
            return buses;
        }

        // GET: api/Buses/5
        [HttpGet("{id}", Name = "Get")]
        public Bus Get(string id)
        {
            var bus = storage.Buses.FirstOrDefault(b => b.Id.Equals(id));
            return bus;
        }
        
        // POST: api/Buses
        [HttpPost]
        public void Post([FromBody]Bus bus)
        {
            bus.Id = Guid.NewGuid().ToString();
            this.storage.Buses.Add(bus);
        }
        
        // PUT: api/Buses/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]Bus bus)
        {
            var candidateBus = this.storage.Buses.FirstOrDefault(b => b.Id.Equals(id));
            if (candidateBus == null)
            {
                candidateBus = new Bus()
                {
                    Id = id,
                    Number = bus.Number,
                    Stations = bus.Stations
                };

                this.storage.Buses.Add(candidateBus);
            }
            else
            {
                candidateBus.Number = bus.Number;
                candidateBus.Stations = bus.Stations;
            }
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            var candidateBus = this.storage.Buses.FirstOrDefault(b => b.Id.Equals(id));
            this.storage.Buses.Remove(candidateBus);
        }
    }
}
