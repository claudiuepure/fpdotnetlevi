﻿using PublicTransport.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var busService = new BusService();
            busService.CreateBus(41);
            busService.AddStationToBus(41, "CUG");
            busService.AddStationToBus(41, "Podu Ros");
            busService.AddStationToBus(41, "Palas");

            busService.PrintBus(41);

            busService.CreateBus(28);
            busService.AddStationToBus(28, "ACB");
            busService.AddStationToBus(28, "Galata");
            busService.AddStationToBus(28, "Podu Ros");

            busService.PrintBus(28);

            var connectionStation = busService.GetConnectionBetween(28, 41);
            System.Console.WriteLine($"Connection " +
                $"from 28 " +
                $"to 41 " +
                $"is: {connectionStation}");
        }
    }
}
