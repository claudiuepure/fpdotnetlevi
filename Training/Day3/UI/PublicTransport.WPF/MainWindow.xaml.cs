﻿using PublicTransport.Application;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PublicTransport.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BusService busService = new BusService();
        ObservableCollection<Bus> buses = new ObservableCollection<Bus>();

        public MainWindow()
        {
            InitializeComponent();
            ReloadBuses();
            busesDataGrid.ItemsSource = buses;
        }

        private void addBus_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBus.Text))
            {
                int bus = Convert.ToInt32(txtBus.Text);
                this.busService.CreateBus(bus);
                ReloadBuses();
                txtBus.Clear();
            }
        }

        private void ReloadBuses()
        {
            var busList = busService.GetBuses().ToList();
            buses.Clear();
            foreach (var bus in busList)
            {
                buses.Add(bus);
            }
        }

        private void addStationClick(object sender, RoutedEventArgs e)
        {
            // busService.AddStationToBus()
        }
    }
}
