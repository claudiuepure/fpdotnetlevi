﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Infrastructure
{
    public class BusRepository
    {
        private readonly string fileName = $"../../database.json";

        public IEnumerable<Bus> GetBusses()
        {
            var busses = this.ReadFile();
            return busses;
        }

        public void AddBus(Bus bus)
        {
            var busses = this.ReadFile();
            busses.Add(bus);
            this.WriteFile(busses);
        }

        public void ModifyBus(Bus modifiedBus)
        {
            var buses = this.ReadFile();
            var existingBus = buses.First(b => b.Number.Equals(modifiedBus.Number));
            var positon = buses.IndexOf(existingBus);
            buses.RemoveAt(positon);
            buses.Insert(positon, modifiedBus);
            this.WriteFile(buses);
        }

        private IList<Bus> ReadFile()
        {
            var json = File.ReadAllText(fileName);
            var busses = JsonConvert.DeserializeObject<List<Bus>>(json);
            return busses ?? new List<Bus>();
        }

        private void WriteFile(IEnumerable<Bus> busses)
        {
            var json = JsonConvert.SerializeObject(busses.ToArray());
            var prettyJson = JValue.Parse(json).ToString(Formatting.Indented);
            File.WriteAllText(fileName, prettyJson);
        }
    }
}
