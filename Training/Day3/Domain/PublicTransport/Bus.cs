﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport
{
    public class Bus
    {
        private IList<Station> stations;

        public Bus(int number)
        {
            this.stations = new List<Station>();
            this.Number = number;
        }

        public int Number { get; }

        public IEnumerable<Station> Stations => this.stations;

        public void AddStation(Station station)
        {
            this.stations.Add(station);
        }

        public void RemoveStation(Station station)
        {
            this.stations.Remove(station);
        }

        public Station GetConnectionTo(Bus otherBus)
        {
            foreach (var station in this.stations)
            {
                foreach (var otherBusStation in otherBus.Stations)
                {
                    if (station.Name.Equals(otherBusStation.Name))
                    {
                        return otherBusStation;
                    }
                }
            }

            return null;
        }
    }
}
