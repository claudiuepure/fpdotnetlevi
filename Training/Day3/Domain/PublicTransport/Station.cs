﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport
{
    public class Station
    {
        public Station(string name)
        {
            this.Name = name;
        }

        public string Name { get; }
    }
}
