﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DummyTests
{
    [TestFixture]
    public class BusTests
    {   //add station
        //remove station
        //get connection

        [Test]
        public void AddStation_NewStation_ToBus_ExpectedStationAdded()
        {
            //Arrange

            //Act

            //Asert
        }
        [Test]
        public void RemoveStation_FromBus_ExpectedStationRemoved()
        {
            //Arrange

            //Act

            //Asert
        }
        [Test]
        public void RemoveStation_FromBusWithNoStation_ExpectedException()
        {
            //Arrange

            //Act

            //Asert
        }
        [Test]
        public void GetConnectionTo_2Buses_WithSameStationInStationList_ShouldReturnStation()
        {
            //Arrange

            //Act

            //Asert

        }
    }
}
