﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DummyTests
{
    [TestFixture]
    public class DummyTests
    {
        [Test]
        public void Sum_10and15_ShouldReturn25()
        {   
            //Arrange
            int nr1 = 10;
            int nr2 = 15;
            int expectedResult = 25;

            //Act
            int actualResult = nr1 + nr2;

            //Asert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void Concatenate_Foo_and_Bar_Expect_FooBar()
        {
            //Arrange
            var myClass = new MyClass();
            string s1 = "Foo";
            string s2 = "Bar";
            string expected = "FooBar";

            //Act
            string result = myClass.Concatenate(s1, s2);
            
            //Asert
            Assert.AreEqual(expected, result);
        }
        
    } 

    class MyClass
    {
        public string Concatenate(string s1,string s2)
        {
            return s1 + s2;
        }
    }
}
