﻿using PublicTransport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport.Application
{
    public class BusService
    {
        private BusRepository busRepository;

        public BusService()
        {
            this.busRepository = new BusRepository();
        }

        public IEnumerable<int> GetBusNumbers()
        {
            var buses = this.busRepository.GetBusses();
            return buses.Select(b => b.Number);
        }

        public IEnumerable<Bus> GetBuses()
        {
            return this.busRepository.GetBusses();
        }

        public void CreateBus(int busNumber)
        {
            var buses = this.busRepository.GetBusses();
            var bussesWithTheSameNumber = buses.Where(b => b.Number.Equals(busNumber));
            if (bussesWithTheSameNumber.Any())
            {
                Console.WriteLine($"Bus {busNumber} already exist.");
            }

            var newBus = new Bus(busNumber);
            this.busRepository.AddBus(newBus);
        }

        public void AddStationToBus(int busNumber, string stationName)
        {
            var buses = this.busRepository.GetBusses();
            var bus = buses.First(b => b.Number.Equals(busNumber));

            var newStation = new Station(stationName);
            bus.AddStation(newStation);

            this.busRepository.ModifyBus(bus);
        }

        public void PrintBus(int busNumber)
        {
            var buses = this.busRepository.GetBusses();
            var bus = buses.First(b => b.Number.Equals(busNumber));
            System.Console.WriteLine("----------------------");
            System.Console.WriteLine($"Bus {bus.Number} has stations:");
            foreach (var station in bus.Stations)
            {
                System.Console.WriteLine(station.Name);
            }
            System.Console.WriteLine("----------------------");
        }

        public string GetConnectionBetween(int busNumberSource, int busNumberTarget)
        {
            var buses = this.busRepository.GetBusses();
            var busSource = buses.First(b => b.Number.Equals(busNumberSource));
            var busTarget = buses.First(b => b.Number.Equals(busNumberTarget));
            var connection = busSource.GetConnectionTo(busTarget);
            return connection.Name;
        }
    }
}
